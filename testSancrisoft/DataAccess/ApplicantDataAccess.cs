﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using testSancrisoft.Models;
using testSancrisoft.ViewModel;

namespace testSancrisoft.DataAccess
{
    public static class ApplicantDataAccess
    {
        internal static AplicanDetailViewModel FindById(int? id)
        {
            try
            {
                using (ModelContext db = new ModelContext())
                {
                    var dato = db.Applicants.Find(id);

                    var View = Mapper.Map<AplicanDetailViewModel>(dato);
                    View.attachments = dato.ApplicantAttachment.Select(x => new ApplicationAtachmentViewModel
                    {
                        idApplicant = x.idApplicant,
                        description = x.description,
                        attachment = x.attachment
                    }).ToList();
                    View.Appointment = dato.Appointment.Description;
                    return View;
                }
            }
            catch (Exception)
            {

                return null;
            }
           

            
        }

        internal static Applicant Save(Applicant applicant)
        {
            using (ModelContext db = new ModelContext())
            {
                db.Applicants.Add(applicant);
                db.SaveChanges();
                return applicant;
            }

          
        }
    }
}