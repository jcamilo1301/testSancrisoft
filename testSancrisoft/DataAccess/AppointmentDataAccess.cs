﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using testSancrisoft.Models;

namespace testSancrisoft.DataAccess
{
    public class AppointmentDataAccess
    {
        internal static List<Appointment> GetAppointment()
        {
            try
            {
                using (ModelContext db = new ModelContext())
                {
                    return db.Appointments.ToList();
                }
                
            }
            catch (Exception)
            {

              return  new List<Appointment>();
            }
          
        }
    }
}