﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using testSancrisoft.Models;

namespace testSancrisoft.DataAccess
{
    public static class ApplicantAttachmentLogic
    {
        internal static void Save(List<ApplicantAttachment> attachments)
        {
            using (ModelContext db = new ModelContext()) {
                db.ApplicantAttachment.AddRange(attachments);
                db.SaveChanges();
            }
        }
    }
}