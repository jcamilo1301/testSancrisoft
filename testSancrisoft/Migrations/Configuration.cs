namespace testSancrisoft.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Models;
    internal sealed class Configuration : DbMigrationsConfiguration<testSancrisoft.DataAccess.ModelContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
          //  AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(testSancrisoft.DataAccess.ModelContext context)
        {
        

                context.Appointments.AddOrUpdate(
                  p => p.Description,
                  new Appointment { Description = "Administrator" },
                  new Appointment { Description = "developer" },
                  new Appointment { Description = "Assistant" }
                );
            context.SaveChanges();

        }
    }
}
