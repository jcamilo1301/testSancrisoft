﻿
function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: {
            lat: -34.653015,
            lng: -58.674850
        }
    });
    var geocoder = new google.maps.Geocoder;
    var infowindow = new google.maps.InfoWindow({ map: map });
    var pos = {
        lat: 0.0,
        lng: 0.0,
    };

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            pos.lat = position.coords.latitude;
            pos.lng = position.coords.longitude;
            geocodeLatLng(geocoder, map, infowindow, pos);
        }, function () {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }





}

function geocodeLatLng(geocoder, map, infowindow, pos) {

    var latlng = pos;
    geocoder.geocode({
        'location': latlng
    }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              //  alert(results[1].address_components[1].long_name + " - " + results[1].address_components[3].long_name)
                $('#country').val(results[1].address_components[3].long_name);
                $('#city').val(results[1].address_components[1].long_name);
            } else {
                window.alert('No hay resultados');
            }
        } else {
            window.alert('Geocoder failed due to: ' + status);
        }
    });
}


function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
}
