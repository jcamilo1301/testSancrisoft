﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace testSancrisoft.Models
{
    public class ApplicantAttachment
    {
        [Key]
        public int idApplicantAttachment { get; set; }

        public int idApplicant { get; set; }
        public virtual Applicant Applicant { get; set; }
        public string attachment { get; set; }
        public string description { get; set; }

      
    }
}