﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace testSancrisoft.Models
{
    public class Appointment
    {
        [Key]
        public int idAppointment { get; set; }

        public string Description { get; set; }

        public virtual ICollection<Applicant> Applicant { get; set; }

    }
}