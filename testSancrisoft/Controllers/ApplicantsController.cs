﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using testSancrisoft.DataAccess;
using testSancrisoft.Logic;
using testSancrisoft.Models;
using testSancrisoft.ViewModel;

namespace testSancrisoft.Controllers
{
    public class ApplicantsController : Controller
    {
        private ModelContext db = new ModelContext();

        // GET: Applicants
        public ActionResult Index()
        {
            var applicants = db.Applicants.Include(a => a.Appointment);              

            return View(applicants.ToList());
            
        }

        // GET: Applicants/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return Redirect("/404.html");
            }
            var applicant = ApplicantLogic.FindById(id); 
            if (applicant == null)
            {
                return Redirect("/404.html");
            }

       
            return View(applicant);
        }

        // GET: Applicants/Create
        public ActionResult Create()
        {
            ViewBag.idAppointment = new SelectList(db.Appointments, "idAppointment", "Description");
            return View();
        }

        // POST: Applicants/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create( ApplicantViewModel view)
        {
            if (ModelState.IsValid)
            {
                if (view.photoC!=null &&( view.photoC.ContentLength >= 1000000 || !view.photoC.ContentType.Contains("image")))
                {
                    ViewBag.idAppointment = new SelectList(AppointmentLogic.GetAppointment(), "idAppointment", "Description", view.idAppointment);
                    ModelState.AddModelError(string.Empty, "photo fiel can not weigh more than 1 bm and it must be a photo");
                    return View(view);
                }
                string ext = ".pdf,.docx,.doc";
                foreach (var item in view.files)
                {

                     if ( item!=null && (item.ContentLength>= 5000000|| (!ext.Contains( Path.GetExtension(item.FileName))
                        && !item.ContentType.Contains("image"))  ))
                    {
                        ViewBag.idAppointment = new SelectList(AppointmentLogic.GetAppointment(), "idAppointment", "Description", view.idAppointment);
                        ModelState.AddModelError(string.Empty, "attachments can not weigh more than 5 bm");
                        return View(view);
                    }
                    
                }
                var folder = "/Content/photos";
                var id=ApplicantLogic.SaveApplicant(view, Server.MapPath(folder));
                ContentEmail contentEmailApplicant = new ContentEmail {
                    message = string.Format(Configuration.messageApplicant, view.name, view.lastName),
                   subject=Configuration.subjectApplicant,
                   to=view.email
                };

                ContentEmail contentEmailAdmin = new ContentEmail
                {
                    message = string.Format( Configuration.messageAdmin, view.name, view.lastName,db.Appointments.Find( view.idAppointment).Description, id, HttpContext.Request.Url.AbsoluteUri),
                    subject = Configuration.subjectAdmin,
                    to = Configuration.emailAdmin
                };



                 ApplicantLogic.SendMail(contentEmailApplicant);
                 ApplicantLogic.SendMail(contentEmailAdmin);
                ViewBag.idAppointment = new SelectList(AppointmentLogic.GetAppointment(), "idAppointment", "Description", view.idAppointment);
                ViewBag.message = "Thank you for applying.Please check your email.";

                return View(new ApplicantViewModel());
            }

            ViewBag.idAppointment = new SelectList(AppointmentLogic.GetAppointment() , "idAppointment", "Description", view.idAppointment);
            return View(view);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
