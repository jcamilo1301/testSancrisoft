﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace testSancrisoft.Logic
{
    public static class Configuration
    {
        public const string emailAdmin = "jcamilo1301@hotmail.com";
        public const string subjectApplicant= "Hemos recibido tu aplicación!";
        public const string subjectAdmin = "Nueva aplicación recibida";
        public const string messageApplicant = "<p>Estimado {0} {1},</p><p>Nos alegra que hayas querido formar parte de nuestro equipo! Hemos recibido satisfactoriamente todos tus datos, los revisaremos y te daremos respuesta a la brevedad posible.</p>";
        public const string messageAdmin = "<p>Una nueva aplicación ha sido recibida.</p> <p>Aplicante:{0} {1}</p> <p>Cargo:{2}</p> <p>Enlace:<a href='{4}/Applicants/details/{3}'>Ir al detalle</a> ";
    }
}