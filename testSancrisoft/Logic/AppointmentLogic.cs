﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using testSancrisoft.DataAccess;
using testSancrisoft.Models;

namespace testSancrisoft.Logic
{
    public static class AppointmentLogic
    {
        internal static List<Appointment> GetAppointment()
        {
           return AppointmentDataAccess.GetAppointment();
        }
    }
}