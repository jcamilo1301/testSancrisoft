﻿using AutoMapper;
using System.Linq;
using System.Web;
using testSancrisoft.DataAccess;
using testSancrisoft.Models;
using testSancrisoft.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Threading.Tasks;
using System.Threading;

namespace testSancrisoft.Logic
{
    public static class ApplicantLogic
    {
        internal static AplicanDetailViewModel FindById(int? id)
        {
            AplicanDetailViewModel applicant = ApplicantDataAccess.FindById(id);
       
            return applicant;

        }

        internal static int SaveApplicant(ApplicantViewModel view, string server)
        {

            var pic = string.Empty;
            var folder = "/Content/photos";

            if (view.photoC != null)
            {
                pic = UploadPhoto(view.photoC, folder,server);
                pic = string.Format("{0}/{1}", folder, pic);
            }

            view.photo = pic;

            var Applicant = Mapper.Map<Applicant>(view);

            Applicant=ApplicantDataAccess.Save(Applicant);
         

            List<ApplicantAttachment> attachments = new List<ApplicantAttachment>();

            var file = string.Empty;
            var folderFile = "/Content/photos";
            if (view.files != null)
            {
                for (int i = 0; i < view.files.Count ; i++)
                {
                    if (view.files[i]!=null)
                    {
                        file = UploadPhoto(view.files[i], folderFile, server);
                        file = string.Format("{0}/{1}", folderFile, file);
                        attachments.Add(new ApplicantAttachment { idApplicant = Applicant.idApplicant, attachment = file, description = view.descripciones[i] });
                    }
                 
                }



            }
            ApplicantAttachmentLogic.Save(attachments);
            return Applicant.idApplicant;
        }

        private static string UploadPhoto(HttpPostedFileBase file, string folder,string server)
        {
            string path = string.Empty;
            string pic = string.Empty;

            if (file != null)
            {
                pic = Path.GetFileName(file.FileName);
                var cont = 1;
                pic = string.Format("Attachment{0}{1}", cont, Path.GetExtension(pic));
                path = Path.Combine(server, pic);
                while (File.Exists(path))
                {
                    cont++;
                    pic = string.Format("Attachment{0}{1}", cont, Path.GetExtension(pic));
                    path = Path.Combine(server, pic);
                }
               
                file.SaveAs(path);
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }
            }

            return pic;
        }


        public static void SendMail(ContentEmail content) {
           
            var message = new MailMessage();
            message.To.Add(new MailAddress(content.to)); //replace with valid value
            message.Subject = content.subject;
            //string mensaje =string.Format( "<p>Estimado {0} {1},</p><p>Nos alegra que hayas querido formar parte de nuestro equipo! Hemos recibido satisfactoriamente todos tus datos, los revisaremos y te daremos respuesta a la brevedad posible.</p>", Applicant.name, Applicant.lastName);
            string mensaje = content.message;
            message.Body = mensaje;
            message.IsBodyHtml = true;
            try
            {
              
                    using (var smtp = new SmtpClient())
                    {
                        smtp.Send(message);

                    }
         
            }
            catch (Exception)
            {
            }
          
            

        }
        //public static async Task SendMailAdmin(ApplicantViewModel Applicant)
        //{

        //    var message = new MailMessage();
        //    message.To.Add(new MailAddress(Configuration.emailAdmin)); //replace with valid value
        //    message.Subject = "Nueva aplicación recibida";
        //    string mensaje = string.Format("<p>Una nueva aplicación ha sido recibida.</p> <p>Aplicante:{0} {1}</p> <p>Cargo:{2} Enlace:<a href='http://http://localhost:63687/applicants/detail/{3}'>Ir al detalle</a> ", Applicant.name, Applicant.lastName,Applicant.idAppointment,25);
        //    message.Body = mensaje;
        //    message.IsBodyHtml = true;
        //    using (var smtp = new SmtpClient())
        //    {
        //        await smtp.SendMailAsync(message);

        //    }
        //}
    }
}