﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace testSancrisoft.Logic
{
    public  class ContentEmail
    {
        public string to { get; set; }
        public string message{ get; set; }
        public string subject { get; set; }
        public string name{ get; set; }
        public int idApplicant { get; set; }
    }
}