﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using testSancrisoft.DataAccess;
using testSancrisoft.Models;
using testSancrisoft.ViewModel;

namespace testSancrisoft
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ModelContext, Migrations.Configuration>());
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Applicant, AplicanDetailViewModel>();
                cfg.CreateMap<ApplicantViewModel, Applicant>();
                cfg.CreateMap<ApplicationAtachmentViewModel, ApplicantAttachment>();
                cfg.CreateMap<ApplicantAttachment, ApplicationAtachmentViewModel>();
            });
       


        }
    }
}
