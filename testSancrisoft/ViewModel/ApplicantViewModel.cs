﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace testSancrisoft.ViewModel
{
    public class ApplicantViewModel
    {


        [Required(ErrorMessage = "The {0} field is required")]
        [Display(Name = "Name")]
        public string name { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        [Display(Name = "Last name")]
        public string lastName { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress,ErrorMessage ="I must be a valid email")]
        public string email { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        [Display(Name = "Telephone")]
        public string telephone { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        [Display(Name = "Birth date")]
        public DateTime birthdate { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "The {0} field is required")]
        [Display(Name = "Biography")]
        public string biography { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        [Display(Name = "Street a")]
        public string streetAddress { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        [Display(Name = "City")]
        public string city { get; set; }

        [Required(ErrorMessage = "The {0} field is required")]
        [Display(Name = "Country")]
        public string country { get; set; }

        [Display(Name = "Postal code")]
        public string postalCode { get; set; }

        [Display(Name = "Photo")]
        public HttpPostedFileBase photoC { get; set; }
        public string photo { get; set; }

        public List<HttpPostedFileBase> files { get; set; }
        public List<string> descripciones { get; set; }
        [Display(Name ="Appointment")]
        public int idAppointment { get; set; }

     




    }
}