﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace testSancrisoft.ViewModel
{
    public class ApplicationAtachmentViewModel
    {
        public int idApplicant { get; set; }
        public string attachment { get; set; }
        public string description { get; set; }
    }
}