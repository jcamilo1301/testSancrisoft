﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using testSancrisoft.Models;

namespace testSancrisoft.ViewModel
{
    public class AplicanDetailViewModel
    {

        public int idApplicant { get; set; }

        [Display(Name = "Name")]
        public string name { get; set; }
        [Display(Name = "Last name")]
        public string lastName { get; set; }
        [Display(Name = "Email")]
        public string email { get; set; }
        [Display(Name = "Telephone")]
        public string telephone { get; set; }
        [Display(Name = "Birth date")]
        public DateTime birthdate { get; set; }
        [Display(Name = "Biography")]
        public string biography { get; set; }
        [Display(Name = "Street address")]
        public string streetAddress { get; set; }
        [Display(Name = "City")]
        public string city { get; set; }
        [Display(Name = "Country")]
        public string country { get; set; }
        [Display(Name = "Postal code")]
        public string postalCode { get; set; }
        [Display(Name = "Photo")]
        public string photo { get; set; }

        [Display(Name = "Appointment")]
        public string Appointment { get; set; }

        public List<ApplicationAtachmentViewModel> attachments { get; set; }

    }
}